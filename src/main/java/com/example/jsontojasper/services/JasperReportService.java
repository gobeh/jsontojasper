package com.example.jsontojasper.services;

import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class JasperReportService {

    public ByteArrayResource generateDataSourceReport(String json)
            throws IOException {

        // get jasper template
        ClassPathResource reportResource = new ClassPathResource("templates/reports/tes_A4.jasper");

        // generate source parameters
        Map<String, Object> reportParameters = new HashMap<>();
        reportParameters.put("jsonSource", json);

        return exportReportToPDF(reportResource.getInputStream(), reportParameters);
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    private ByteArrayResource exportReportToPDF(InputStream targetStream, Map<String, Object> parameters) {
        try {
            JasperPrint jasperPrint = JasperFillManager.fillReport(targetStream, parameters);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);

            byte[] reportContent = outputStream.toByteArray();
            return new ByteArrayResource(reportContent);
        } catch (Exception e) {
            log.error("Exporting report to PDF error: ", e);
            return null;
        }
    }
}
