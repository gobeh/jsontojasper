package com.example.jsontojasper.controllers;

import com.example.jsontojasper.services.JasperReportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;

@Slf4j
@Controller
public class ReportController {

    @Autowired
    private JasperReportService jasperReportService;

    @PostMapping(value = "/generate-pdf")
    public ResponseEntity<ByteArrayResource> generateSimplePDFReport(@RequestParam String jsonUrl) {
        log.info("Payload for generating simple PDF report: {}", jsonUrl);
        try {
            ByteArrayResource byteArrayResource = jasperReportService.generateDataSourceReport(jsonUrl);

            return ResponseEntity
                    .ok()
                    .contentLength(byteArrayResource.contentLength())
                    .header("Content-type", "application/pdf")
                    .header("Content-disposition", "inline; filename=\"" +
                            (byteArrayResource.getFilename() == null ? "hasil-convert" : byteArrayResource.getFilename()) + ".pdf" + "\"")
                    .body(byteArrayResource);
            //return new ResponseEntity<>(byteArrayResource, HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }
}
